<select id='<?php echo $_POST["nomid"]; ?>' name='<?php echo $_POST["nomid"]; ?>' >

<?php
require_once ('notaires_fonctions.php');

sessionCheck();

if (isAdmin() || isGestionnaire())
{
    $year = getPost('valeur');
    
    if ($year > 0) 
    {
	   $req="SELECT distinct month(date) FROM `log_membre` WHERE year(date)=".sql_escape($year)." ORDER BY month(date) ASC;";
	   $res = $connect->query($req);
	   while($mm = $res->fetch())
	   {
	       $moisLisible = ucfirst(moisEnClair($mm[0]));
?>
			<option value="<?php echo $mm[0]; ?>" selected="true"><?php echo $moisLisible; ?></option> 
<?php 
	   }
    }
}
?>
</select>