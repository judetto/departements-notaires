{# Message confirmation déverrouillage du compte #}

<div id="message_deverouille">
  Votre compte a bien été déverrouillé. Vous pouvez maintenant vous reconnecter à l'application en retournant à
  l'accueil. 
  <br />
  <a class="deverouillage" href="index.php">Retour à l'accueil</a>'
</div>  