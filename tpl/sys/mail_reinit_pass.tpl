{# Corps mail réinit mot de passe #}

<style>
#encart_aide {
  background-color: #3F51B5;
  padding-left: 15px;
  padding-right: 15px;
  padding-top: 20px;
  padding-bottom: 15px;
}

#encart_message {
  background-color: #fafafa;
  padding-left: 15px;
  padding-right: 15px;
  padding-top: 20px;
  padding-bottom: 15px;
  color: #9b9b9b;
}

.corps {
  font-family: 'verdana';
  font-size: 80%;
}
</style>

<p class="corps">
  Bonjour,<br />
  <br /> Merci de suivre ce lien afin de réinitialiser votre mot de passe :<br />
  <br />
  <a href="{{url_reinit}}">Réinitialisation du mot de passe</a>
  <br />
  Si vous n'avez pas demandé de réinitialisation de mot de  passe, vous pouvez ignorer ce message.
  <br />
  <br />
  <br /> Cordialement,
  <br />
  <b>{{config.nom_application}}</b>
</p>

