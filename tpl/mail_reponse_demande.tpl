{# Corps mail réponse demande en cours #}

<style>
.corps{
    font-family:'verdana';
    font-size:80%%;
}
.signature{
    font-family:'verdana';
    font-size:75%;font-weight:bold;
}
</style>

<p class=corps>
    Bonjour,
    <br />
    <br /> Il semble qu'il existe une demande en cours d'instruction au nom de {{prenom}} {{nom}}
    <br /> dont la date de naissance est le {{date_naissance}} et dont le décès est survenu le : {{date_deces}}.
    <br />
    <br /> Nous vous informons qu’une étude notariale a entamé une recherche de créances sur succession pour cette personne.
    <br /> Cette demande émane de l'étude : {{libelle_notaire}}, dont le mail de contact est : {{mail_notaire}}
    <br />
    <br />
    <br /> Cordialement,
    <br />
</p>
<p class="signature"> L'équipe {{config.nom_application}}
    <br /> {{config.nom_departement_long | raw}}
</p>
