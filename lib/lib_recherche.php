﻿<?php
/**
 * Bibliothèque regroupant les fonctions liées à la recherche des dossiers
 */


/**
 * Exécute la recherche et renvoie un code pour le type de résultat.
 * Si un des paramètres en entrée n'est pas spécifié, renvoie 'PARAM_VIDE'.
 * 
 * @param array $criteres contient les clés/valeurs pour la recherche 
 *    Clés obligatoires : 'nom_usage', 'prenom', 'date_deces', 'date_acte', 'lieu_deces', 'date_naissance'
 *    Clés optionnelles : 'prenomd', 'prenomt', 'nom_civil', 'destinataire_etude'
 *    
 * @return String une chaîne indiquant le type du résultat trouvé
 *    'PARAM_VIDE' : un des paramètres obligatoires est manquant ou vide
 */
function notairesSearch($criteres)
{
  global $connect;
  
  $reponseNotaire = REPONSE_ERREUR;
  $message = ""; // message retour
  $err_message = ""; // message d'erreur interne
  
  // Paramètres obligatoires
  $nom_usage = getValue($criteres, 'nom_usage');
  $prenom = getValue($criteres, 'prenom');
  $date_deces = getValue($criteres, 'date_deces');
  $date_acte = getValue($criteres, 'date_acte');
  $lieu_deces = getValue($criteres, 'lieu_deces');
  $date_naissance = getValue($criteres, 'date_naissance');
  
  // Paramètres optionnels
  $prenomd = getValue($criteres, 'prenomd');//Pour le recherche
  $prenomdlog = getValue($criteres, 'prenomd');//Pour le log
  $prenomt = getValue($criteres, 'prenomt');//Pour le recherche
  $prenomtlog= getValue($criteres, 'prenomt');//Pour le log
  $nom_civil = getValue($criteres, 'nom_civil');//Pour le recherche
  $nom_civillog = getValue($criteres, 'nom_civil');//Pour le log
  $destinataire_etude = trim(getValue($criteres, 'destinataire_etude'));
  
  if($prenomd==""){$prenomd ="!vide!";}
  if($prenomt==""){$prenomt ="!vide!";}
  if($nom_civil==""){$nom_civil="!vide!";}
  
  // Variable locale
  $mail_gestion = config("mail_gestion");
  $mail_utilisateur = getSession('mailNotaire');
  $num_ind = 0; // id de l'individu si trouvé
  
  // Tous les paramètres obligatoires ne sont pas spécifiés
  if ( empty($nom_usage) || empty($prenom) || empty($date_deces) || empty($date_acte) || empty($lieu_deces) 
      || empty($date_naissance)) {
    if (hasDebug(3)) {
      logDebug("nom_usage = [$nom_usage]");
      logDebug("prenom = [$prenom]");
      logDebug("date_deces = [$date_deces]");
      logDebug("date_acte = [$date_acte]");
      logDebug("lieu_deces = [$lieu_deces]");
      logDebug("date_naissance = [$date_naissance]");
    }
    return "PARAM_VIDE";    
  }
  
  // Log
  $log_libelleNotaire = getSession('libelleNotaire');
  if (!$log_libelleNotaire) $log_libelleNotaire = "Manque Libellé";
  
  // Les paramètres transformés pour interrogation SQL sont préfixés avec 'sql_'
  
  // Le nom est stocké en Majuscule dans table individus
  $sql_nom_usage = sql_escape(trim(strtoupper($nom_usage))); // les apostrophes ' sont présents au début et fin

  // Le prénom est cherché avec les accents saisis
  $sql_prenom = sql_escape(trim($prenom)); // ne pas refaire le decodage utf-8
  $sql_prenom_sans = sql_escape(removeAccent(trim($prenom))); 
  
  // Crtière ouvert sur le prénom : 3 caractères + % 
  $sql_prenom_debut = sql_escape(mb_substr(trim($prenom), 0, 3,'UTF-8')."%");
      
  // transformation des dates pour compatibilité dans la base
  list ($jour_dNotaire, $mois_dNotaire, $annee_dNotaire) = preg_split('/[.\-\/]+/', $date_deces);
  $date_decesNotaire = $annee_dNotaire . '-' . $mois_dNotaire . '-' . $jour_dNotaire;
  $lieu_decesNotaire = addslashes($lieu_deces);
  list ($jour_aNotaire, $mois_aNotaire, $annee_aNotaire) = preg_split('/[.\-\/]+/', $date_deces);
  $date_acteNotaire = $annee_aNotaire . '-' . $mois_aNotaire . '-' . $jour_aNotaire;
  
  $sql_prenom_deux=sql_escape(trim($prenomd));
  $sql_prenom_trois=sql_escape(trim($prenomt));
  
  $prenomdNotaire = trim(removeAccent($prenomdlog));
  $prenomtNotaire = trim(removeAccent($prenomtlog));
  
  $sql_nom_civil = sql_escape(trim(strtoupper($nom_civil)));
  $sql_nom_civillog = sql_escape(trim(strtoupper($nom_civillog)));
  
  // Reformatte la date pour interrogation SQL
  list ($jourNotaire, $moisNotaire, $anneeNotaire) = preg_split('/[.\-\/]+/', $date_naissance);
  // $date_naissNotaire est utilisé dans log_recherche qui attend la forme sans guillemet
  $date_naissNotaire = $anneeNotaire . '-' . $moisNotaire . '-' . $jourNotaire;  
  // Les variables ci-dessous sont utilisées avec les guillemets
  $jourNotaire  = sql_escape($jourNotaire);
  $moisNotaire  = sql_escape($moisNotaire);
  $anneeNotaire = sql_escape($anneeNotaire);
  
  $log_num_rechNotaire = newSearchId();
  $log_dateCourante = date("Y-m-d H:i:s");

  // Paramètres pour la résolution des templates
  $search = [
      "destinataire_etude"  => $destinataire_etude,
  		"nom"                 => strtoupper($nom_usage),
  		"nom_civil"           => strtoupper($nom_civillog),
      "prenom"              => $prenom,
      "prenomd"             => $prenomdlog,
      "date_naissance"      => $date_naissance,
      "date_deces" => $date_deces,
      "libelle_notaire"     => $log_libelleNotaire,
      "mail_notaire"        => $mail_utilisateur,
      "maison_departementale"     => "",
      "tel_maison_departementale" => "",
  		"sexe"											=> "",
  		"naissance"									=> "née",
      
  ];
  $maison_departementale      = "";
  $tel_maison_departementale  = "";
  $mail_maison_departementale = "";
  $sexe 											= "";
  $naissance									= "";
  $sql_nom_usage_2						= str_replace('-', ' ', $sql_nom_usage);
  $sql_nom_civil_2						= str_replace('-', ' ', $sql_nom_civil);
  // *****************************************************************************************************************
  // Passe 1 : Recherche du nombre de réponse seulement avec les conditions
  // - ouverte sur prenom (%)
  // - précise sur deuxième prénom
  // - contrainte sur date de naissance complète
  // *****************************************************************************************************************
  $where_annee = "("
								. "("
  								."("
  										."    nom_usage IN (" . $sql_nom_usage_2. ", " . $sql_nom_civil_2.", ". $sql_nom_civil . " , " . $sql_nom_usage . ")"
  												." OR nom_civil IN( " . $sql_nom_usage_2. ", " . $sql_nom_civil_2. ",". $sql_nom_civil . " , " . $sql_nom_usage . ")"
  								.")"
  								." AND "
  								."("
  									 ."    prenom IN  (" . $sql_prenom . "," . $sql_prenom_deux. "," . $sql_prenom_trois. ")"
  									 ." OR prenomd IN  (" . $sql_prenom . "," . $sql_prenom_deux. "," . $sql_prenom_trois. ") "
  									 ." OR prenomt IN  (" . $sql_prenom . "," . $sql_prenom_deux. "," . $sql_prenom_trois. ")"
  								.")"
  							 .")"
  							 . "OR "
  							 ."("
  							  ."("
  								 ."    nom_usage IN (" . $sql_nom_usage . ", " . $sql_nom_civil . ") "
  								 ." OR nom_civil IN (" . $sql_nom_usage . ", " . $sql_nom_civil . ") "
  								.")"
  								." AND prenom like " . $sql_prenom_debut
  							 .")"
  						 . ")"
  						 . " AND annee_naissance = " . $anneeNotaire;

  
  $where_complet = $where_annee  . " AND mois_naissance  = " . $moisNotaire. " AND jour_naissance  = " . $jourNotaire;

  $req_count = "SELECT count(distinct(num_ind)) as nb FROM individus WHERE $where_complet";

  $result_ = $connect->query($req_count);
  $valid_ = $result_->fetch();
  $result_->closeCursor();
  
  if ( hasDebug(2)) {
    logDebug("Passe 1 requ_ : $req_count");
  }
  
  $type_sortie_pdf = null;
  $mail_copie = null; // deuxième destinataire du mail
  
  // On test si la personne peut être identifiée formellement si oui on recherche le type d'offre.
  // ----------------------
  // Passe 1 - Une seule réponse
  // ----------------------
  if ($valid_["nb"] == 1) {
    // Réeécute la requête pour lire les champs
    $req_read = "SELECT * FROM individus WHERE $where_complet";
    $spy = "";
    $spo = "";
    $result_ = $connect->query($req_read);
    
    
    while ($val = $result_->fetch()) {
      $sexe = $val['sexe'];
      
      // Si le code est 1SEXTREC alors il y à possibilité de récupération
      if ($val['code'] == '1SEXTREC') {
        $spy = 'R';
        $num_ind = $val['num_ind'];
        $maison_departementale = $val['mdr'];
        $tel_maison_departementale = $val['telephone'];
        $mail_maison_departementale = ($val['mail_mdr']);
      }            
      // Si le code est 1SEXTNONR alors il n'y à pas possibilité de récupération mais il peut y avoir de l'indu.
      elseif ($val['code'] == '1SEXTNONR') {
        $spo = 'N';
        $num_ind = $val['num_ind'];
        $maison_departementale = ($val['mdr']);
        $tel_maison_departementale = $val['telephone'];
        $mail_maison_departementale = ($val['mail_mdr']);
        } else {
        $maison_departementale = "";
        $tel_maison_departementale = "";
        $mail_maison_departementale = "";
      }
      // Renseigne infos complémentaires pour template
      if ($maison_departementale== 'ARBRESLE') {
      	$maison_departementale= "L'ARBRESLE";
      }
      $search["maison_departementale"] = $maison_departementale;
      $search["tel_maison_departementale"] = $tel_maison_departementale; 
      if ($sexe == "M") {
      	$sexe = 'Monsieur';
      	$naissance = 'né';
      } else if ($sexe == "F") {
      	$sexe = 'Madame';
      	$naissance = 'née';
      } else {
      	$sexe = "";
      	$naissance = 'née';
      }
      $search["naissance"] = $naissance;
      $search["sexe"] = $sexe;
      
    }
    // On teste le fait qu'il y a de la récupération mais pas d'indu
    if (($spy == 'R' && $spo != 'N') || ($spy == 'R' && $spo == 'N')) {
      $reponseNotaire = REPONSE_RECUPERATION;
      $type_sortie_pdf = "aide_recup";
      $mail_copie = $mail_gestion; // copie aux gestionnaires

      log_recherche($log_num_rechNotaire, $log_libelleNotaire, $sql_nom_usage, $sql_prenom, $date_naissNotaire, 
        $log_dateCourante, $reponseNotaire, $date_decesNotaire, $lieu_decesNotaire, $date_acteNotaire, $num_ind, 
      		$maison_departementale, $tel_maison_departementale, $sql_nom_civillog, $prenomdNotaire, $prenomtNotaire, $destinataire_etude);
    }        
    // On teste le fait qu'il n'y a pas de récupération mais de l'indu
    elseif ($spy != 'R' && $spo == 'N') {
      $reponseNotaire = REPONSE_INDUS_PROBABLE;
      $type_sortie_pdf = "aide_recup";
      $mail_copie = $mail_maison_departementale; // copie à la Maison Départementale
      
      log_recherche($log_num_rechNotaire, $log_libelleNotaire, $sql_nom_usage, $sql_prenom, $date_naissNotaire, 
        $log_dateCourante, $reponseNotaire, $date_decesNotaire, $lieu_decesNotaire, $date_acteNotaire, $num_ind, 
      		$maison_departementale, $tel_maison_departementale, $sql_nom_civillog, $prenomdNotaire, $prenomtNotaire, $destinataire_etude);
      
    }        
    // S'il n'y à ni l'un ni l'autre, la recherche est considérée comme infructueuse
    else {
      $num_ind = 0; // il faut remettre à zéro l'id individu
      $reponseNotaire = REPONSE_INCONNU;
      $type_sortie_pdf = "dossier_inconnu";
      $mail_copie = null; // pas de copie
      }
  } // Un individu trouvé    
  // ----------------------
  // Passe 1 - Aucune réponse
  // ----------------------
  elseif ($valid_["nb"] == 0) {
    // ***************************************************************************************************************
    // Passe 2 : La requête est relancée sur l'année de naissance seulement
    // ***************************************************************************************************************
    $req_count = "SELECT count(distinct(num_ind)) as nb FROM individus WHERE $where_annee";
    $result_ = $connect->query($req_count);
    $valid_ = $result_->fetch();
    $result_->closeCursor();
    
    if ( hasDebug(2)) {
        logDebug("Passe 2 requ_ : $req_count");
    }
    
    // S'il y a un individu trouvé, la réponse est ambigue
    if ($valid_["nb"] >= 1) {
      $reponseNotaire = REPONSE_AMBIGU;
      // Traitement ci-après
    } 
    else {
      // Fix #30
      // ***************************************************************************************************************
      // Passe 3 : La requête est relancée sans les prenoms mais avec la date de naissance précise
      // ***************************************************************************************************************
      $req_count = "SELECT count(distinct(num_ind)) as nb FROM individus WHERE "
        ."("
          ."nom_usage IN (" . $sql_nom_usage_2. ", " . $sql_nom_civil_2.", ". $sql_nom_civil . " , " . $sql_nom_usage . ")"
          ." OR nom_civil IN( " . $sql_nom_usage_2. ", " . $sql_nom_civil_2. ",". $sql_nom_civil . " , " . $sql_nom_usage . ")"
        .")"
        . " AND annee_naissance = " . $anneeNotaire . " AND mois_naissance = " . $moisNotaire. " AND jour_naissance  = " . $jourNotaire;
                       
      $result_ = $connect->query($req_count);
      $valid_ = $result_->fetch();
      $result_->closeCursor();
        
      if ( hasDebug(2)) {
        logDebug("Passe 3 requ_ : $req_count");
      }
        
      // S'il y a au moins un individu trouvé, la réponse est ambigue
      if ($valid_["nb"] >= 1) {
         $reponseNotaire = REPONSE_AMBIGU;
         // Traitement ci-après
      }
      else {
        $reponseNotaire = REPONSE_INCONNU;
        $type_sortie_pdf = "dossier_inconnu";
        // Traitement ci-après
      }
    }
  }
  // ----------------------
  // Passe 1 - Plusieurs individus trouvés
  // ----------------------
  else {
    $reponseNotaire = REPONSE_AMBIGU;
    // Traitement ci-après
  }

  // ----------------------
  // REPONSE_AMBIGU est possible dans plusieurs cas, le traitement est mutualisé ici
  // ----------------------
  if ( $reponseNotaire==REPONSE_AMBIGU ) {
    log_recherche($log_num_rechNotaire, $log_libelleNotaire, $sql_nom_usage, $sql_prenom, $date_naissNotaire,
        $log_dateCourante, $reponseNotaire, $date_decesNotaire, $lieu_decesNotaire, $date_acteNotaire, $num_ind,
    		$maison_departementale, $tel_maison_departementale, $sql_nom_civillog, $prenomdNotaire, $prenomtNotaire, $destinataire_etude);
    
    list ($mail_sujet, $mail_corps) = getEmailContent($reponseNotaire, $search);
    sendResponseEmail($mail_utilisateur, $mail_gestion, $mail_sujet, $mail_corps, null);
  }
  
  // ----------------------
  // REPONSE_INCONNU : élargissement aux demande possible dans plusieurs cas, le traitement est mutualisé ici
  // ----------------------
  if ( $reponseNotaire==REPONSE_INCONNU ) {
    // Le log est toujours écrit après l'exécution de la requête SQL
    log_recherche($log_num_rechNotaire, $log_libelleNotaire, $sql_nom_usage,
        $sql_prenom, $date_naissNotaire, $log_dateCourante, $reponseNotaire, $date_decesNotaire,
        $lieu_decesNotaire, $date_acteNotaire, $num_ind, $maison_departementale, $tel_maison_departementale,
    		$sql_nom_civillog, $prenomdNotaire, $prenomtNotaire, $destinataire_etude);
    
    // Une vérification est faite sur les demandes en cours pour signaler éventuellement au gestionnaire
    // qu'une recherche a été réalisée sur un dossier en cours  
    $req_demande = "SELECT count(distinct(num_ind)) as nb FROM demande WHERE "
				."("
    			."("
    				."("
    					."    nom_usage IN (" . $sql_nom_usage_2. ", " .  $sql_nom_civil_2. "," . $sql_nom_civil . " , " . $sql_nom_usage . ") "
    					." OR nom_civil IN( " . $sql_nom_usage_2. ", " . $sql_nom_civil_2. "," . $sql_nom_civil . " , " . $sql_nom_usage . ")"
    				.") "
    				."AND "
    				." ("
    					."    prenom IN  (" . $sql_prenom . "," . $sql_prenom_sans. ") "
    					." OR prenom_sans IN  (" . $sql_prenom . "," . $sql_prenom_sans. ")"
    					." OR prenom like " . $sql_prenom_debut
    				.")"
    			. ") "
    		. ")"
    		. " AND annee_naissance = " . $anneeNotaire
    		. " AND mois_naissance  = " . $moisNotaire
    		. " AND jour_naissance  = " . $jourNotaire;
    
    $result = $connect->query($req_demande);
    $valid = $result->fetch();

    if (hasDebug(2)) {
      logDebug("req_demande : $req_demande");
    }

    // Une demande existe, envoie l'email aux gestionnaires uniquement
    if ($valid["nb"] == 1) {
      if (hasDebug(1)) {
        logDebug("Une demande existe");
      }
      list ($mail_sujet, $mail_corps) = getEmailContent(REPONSE_INT_DEMANDE, $search);
      try {
        sendResponseEmail($mail_gestion, null, $mail_sujet, $mail_corps, null);
      } catch (Exception $e) {
        $err_message = "Erreur envoi email : " .$e->getMessage();
      }
    }
  } // REPONSE_INCONNU
  
  // ----------------------
  // Réponse mail + PDF au notaire commune à  REPONSE_INCONNU, REPONSE_RECUPERATION
  // ----------------------
  if ( $reponseNotaire==REPONSE_RECUPERATION || $reponseNotaire==REPONSE_INDUS_PROBABLE || $reponseNotaire==REPONSE_INCONNU ) {
    $pdf_path = null;
    try {
      // Génère le PDF
      $corps_pdf = getLetterContent($reponseNotaire, $search);
      
      $pdf_path = generer_pdf($log_num_rechNotaire, $type_sortie_pdf, $corps_pdf);
      if (hasDebug(2)) {
      	logDebug("fichier_pdf : ".$pdf_path);
      }
    } catch (Exception $e) {
      $err_message = "Erreur génération PDF : " .$e->getMessage();
    }
    if ($pdf_path) {
      // Envoie email au notaire
      list ($mail_sujet, $mail_corps) = getEmailContent($reponseNotaire, $search);
      try {
        sendResponseEmail($mail_utilisateur, $mail_copie, $mail_sujet, $mail_corps, $pdf_path);
        unlink($pdf_path);
      } catch (Exception $e) {
        $err_message = "Erreur envoi email : " .$e->getMessage();
      }
    }
    
    if (hasDebug(2)) {
      // Ne supprime pas le fichier PDF en debug
    } else {
      // Supprime le fichier PDF temporaire
      if ($pdf_path) {
        try {
        	if( file_exists( $pdf_path ) ){
          	unlink($pdf_path);
        	}
        } catch (Exception $e) {
          // TODO tracer erreur suppression fichier PDF
        }
      }
    }
  }
  
  // ------------------------------------------
  // Affecte le message retour de l'écran suivant la réponse
  // ------------------------------------------
  switch($reponseNotaire) {
    case REPONSE_RECUPERATION:
    case REPONSE_INDUS_PROBABLE:
      $message = "Cette personne est <b>connue</b> de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.";
      break;
    case REPONSE_INCONNU:
      $message = "Cette personne est <b>inconnue</b> de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.";
      break;
    case REPONSE_AMBIGU:
      $message = "Les éléments que vous avez saisis ne permettent pas d'établir avec certitude l'identité de la personne.<br/>
        Les services du département ont pris en charge votre demande et vous répondront dans les meilleurs délais. <br/>";
      break;
    default : // C'est un message d'erreur
      // TODO tracer/logger/avertir
      $message = "### Une erreur inattendue est survenue";
  }
  if ( $err_message != "") {
    $message = "ERREUR INTERNE : $err_message <br/><br/>" . $message;
  }
  return $message;

} // notairesSearch

/**
 * Stocke la recherche en base de données dans la table log_recherche. 
 * ATTENTION : ce n'est pas qu'une statistique, la génération des courriers PDF se base sur le contenu de cette table.
 * ATTENTION : un paramètre nommé avec le prefixe 'sql_' est supposé déjà bien formatté ; sinon, on applique sql_escape()
 * avant de l'utiliser dans cette fonction 
 * @param $num_rech
 * @param $libelle
 * @param $sql_nom_usage
 * @param $sql_prenom
 * @param $date_naiss
 * @param $date
 * @param $reponse
 * @param $date_deces
 * @param $lieu_deces
 * @param $date_acte
 * @param $id_individus
 * @param $mdr
 * @param $tel_mdr
 * @param $sql_nom_civil
 * @param $prenomd
 * @param $prenomt
 * @param $dest_etude
 */
function log_recherche($num_rech, $libelle, $sql_nom_usage, $sql_prenom, $date_naiss, $date, $reponse, $date_deces, $lieu_deces, 
    $date_acte, $id_individus, $mdr = 'Aucun', $tel_mdr = 'Aucun', $sql_nom_civil, $prenomd, $prenomt, $dest_etude)
{
  global $connect;
  
  $etude = getSession('loginNotaire');

  $prenomtout = $prenomd;
  if ($prenomt != null) {
    $prenomtout .= "####";

    if (mb_substr_count($prenomt, ",") > 1) {
      $pp = explode(",", $prenomt);
      foreach ($pp as $ff) {
        $prenomtout .= $ff . "####";
      }

      $prenomtout = substr($prenomtout, 0, - 4);
    } else {
      $prenomtout .= $prenomt;
    }
  }
  // Dans le cas d'une réponse ambigue, on n'a pas den uméro de PDF
  $lien_pdf = $num_rech;
  if ($reponse == REPONSE_AMBIGU) {
    $lien_pdf = 'Pas de pdf';
  }
  $requ_insert = "INSERT INTO log_recherche (id, id_recherche, nom_etude, libelle, nom_usage, nom_civil,prenom, date_naissance"
      .", lien_pdf, date, reponse, date_deces, lieu_deces, date_acte, id_individus, mdr, tel_mdr, prenom2, dest_etude) VALUES (NULL, "
      . $num_rech . "," . sql_escape($etude). ", " . sql_escape($libelle) . "," . $sql_nom_usage . ", " 
      . $sql_nom_civil  . "," . $sql_prenom . "," 
      		. sql_escape($date_naiss) . "," . sql_escape($lien_pdf)   . "," . sql_escape($date) . "," . $reponse . "," 
      . sql_escape($date_deces) . "," . sql_escape($lieu_deces) . "," 
      . sql_escape($date_acte)  . "," . $id_individus . "," . sql_escape($mdr) . "," . sql_escape($tel_mdr) . "," 
      . sql_escape($prenomtout) . "," . sql_escape($dest_etude) . ")";
  
  if (hasDebug(2)) {
    logDebug("log_recherche : $requ_insert");
  }
  $result = $connect->query($requ_insert);
}

/**
 * Renvoie le sujet et le corps adapté suivant le type de réponse
 * @param $reponse_code
 * @param Array $search données recherchées contenant les clés "destinataire_etude", "nom", "prenom"
 * @return list(subject, body) 
 */
function getEmailContent($reponse_code, $search) 
{
  $mail_sujet = "";
  $mail_corps = "";
  
  // Passe la config au template
  $search["config"] = configAll();

  // Sujet commun à plusieurs messages
  $mail_sujet = sprintf("[%s] Destinataire : %s - Succession %s %s %s",
      config('nom_application'),
      $search['destinataire_etude'],
      $search['prenom'],
      $search['nom'],
      config('mess_prod'));
  
  switch($reponse_code) {
    
    case REPONSE_RECUPERATION:
    case REPONSE_INDUS_PROBABLE:
    	$mail_corps = templateRender("mail_reponse_recup.tpl", $search);
      break;
      
    case REPONSE_INCONNU :
    	$mail_corps = templateRender("mail_reponse_inconnu.tpl", $search);
      break;
      
    case REPONSE_AMBIGU :
      $mail_sujet = sprintf("[%s] Destinataire : %s - Recherche succession %s %s %s",
          config('nom_application'),
          $search['destinataire_etude'],
          $search['prenom'],
          $search['nom'],
          config('mess_prod'));
      
      $mail_corps = templateRender("mail_reponse_ambigu.tpl", $search);
      
      break;
      
    case REPONSE_INT_DEMANDE :
      $mail_sujet = sprintf("[%s] - Instruction %s",
          config('nom_application'),
          config('mess_prod'));
      
      $mail_corps = templateRender("mail_reponse_demande.tpl", $search);
      
      break;
      
  }
  return [$mail_sujet, $mail_corps]; 
}

/**
 * Renvoie le corps du courrier PDF en fonction de la réponse
 * @param $reponse_code
 * @param Array $search données recherchées contenant les clés "destinataire_etude", "nom", "prenom", etc.
 * @return le corps du courrier PDF
 */
function getLetterContent($reponse_code, $search)
{
  $corps_pdf = "";
  
  switch($reponse_code) {
    case REPONSE_RECUPERATION :
      $corps_pdf = templateRender('recup.tpl', $search);
      break;

    case REPONSE_INDUS_PROBABLE :
      $corps_pdf = templateRender('indus.tpl', $search);
      break;
    
    case REPONSE_INCONNU :
      $corps_pdf = templateRender('inconnu.tpl', $search); 
      break;
  }
  return $corps_pdf;
}



?>
