# Création de la BDD notaires

Exécuter le script `notaires.sql` dans votre BDD, ceci aura pour effet de créer la base de données, constituée de 6 tables à vide.

Le compte admin de l'application est :
* Login : admin
* Mot de passe : admin_notaires

Les mots de passes sont cryptés en md5.


# Description des tables

## demandes

La table `demandes` réunit tous les individus dont la ou les demandes d'ASG (Aide Social Générale), d'ADPA (Allocation 
Départementale Personnalisée d'Autonomie) ou de PCH (Prestation de Compensation d'Handicap) sont en cours d'instruction.

* `id` est auto incrémenté, propre à la table
* `num_id` est le numéro individu issu de l'application tiers, propre à elle
* `sexe` est le sexe de la personne (M ou F)
* `nom_usage` est le nom d'usage (marital) de la personne (accepte : - espace è é ô ö ï ë ä ç ')
* `nom_usage_sans` est le nom d'usage sans les accents  (accepte : - espace ')
* `nom_civil` est le nom de naissance de la personne (accepte : - espace è é ô ö ï ë ä ç ')
* `prenom` est le prénom de la personne (accepte : - espace è é ô ö ï ë ä ç)
* `prenom_sans` est le prénom de la personne sans les accents (accepte : - espace)
* `annee_naissance` est l'année de naissance de la personne (YYYY)
* `mois_naissance` est le mois de naissance de la personne 
* `jour_naissance` est le jour de naissance de la personne

___

## individus

La table `individus` créunit tous les individus bénéficiant, ou ayant bénéficié, de prestations d'ASG (Aide Social Générale), 
d'ADPA (Allocation Départementale Personnalisée d'Autonomie) ou de PCH (Prestation de Compensation d'Handicap).

* `id` est auto incrémenté, propre à la table
* `num_id` est le numéro individu issu de l'application tiers, propre à elle
* `sexe` est le sexe de la personne (M ou F)
* `nom_usage` est le nom d'usage (marital) de la personne (accepte : - espace è é ô ö ï ë ä ç ')
* `nom_usage_sans` est le nom d'usage sans les accents (accepte : - espace ')
* `nom_civil` est le nom de naissance de la personne (accepte : - espace è é ô ö ï ë ä ç ')
* `prenom` est le prénom de la personne (accepte : - espace è é ô ö ï ë ä ç)
* `prenomd` est le prénom sans accent
* `prenomt` est non utilisé ici
* `annee_naissance` est l'année de naissance de la personne (YYYY)
* `mois_naissance` est le mois de naissance de la personne
* `jour_naissance` est le jour de naissance de la personne

* `adresse` est l'adresse du référent administratif du dossier de la personne 
* `mdr` est le nom du référent administratif du dossier de la personne 
* `telephone` est le téléphone du référent administratif 
* `mail_mdr` est le mail du référent administratif 

* `libelle` est l'aide accordée à la personne (exemple : Charges exceptionnelles (mensuel), Fauteuil roulant (FDCH),
Aides techniques (FDCH), ADPA à domicile, Aide ménagère couple PH, etc)
* `code` est un code indiquant si l'aide est récupérable ou non 
    - 1SEXTREC : recupérable
    - 1SEXTNONR : non récupérable

___

## log_membre

La table `log_membre` trace l'ensemble des connexions à l'application

* `id` est auto incrémenté, propre à la table
* `id_membre` est l'id du membre connecté
* `login` est le login du membre connecté
* `libelle` est le nom du membre connecté
* `adresse_mail` est l'adresse mail du membre connecté
* `date` est la date de connexion lors de l'authentification dans l'application (YYYY-MM-DD HH: mm:ss)

___

## log_recherche

La table `log_recherche` trace toutes les recherches réalisées par les membres de l'application.

* `id` est auto incrémenté, propre à la table
* `id_recherche` identifiant de la recherche, généré aléatoirement
* `nom_etude` est le login du membre ayant réalisé la recherche 
* `libelle` est le nom du membre ayant réalisé la recherche 
* `nom_usage` est le nom d'usage de la personne recherchée (accepte : - espace è é ô ö ï ë ä ç ')
* `nom_civil` est le nom de naissance de la personne recherchée (accepte : - espace è é ô ö ï ë ä ç ')
* `prenom` est le prénom de la personne recherchée (accepte : - espace è é ô ö ï ë ä ç)
* `prenom2` est le champ reprenant les autres prénoms de la personne recherchée
* `dest_etude` est un champ libre pour le membre réalisant la recherche dans l'étude notariale
* `date_naissance` est la date de naissance de la personne recherchée (YYYY-MM-DD)
* `lien_pdf` identique à id_recherche sinon "pas de pdf"
* `date` est la date de la réalisation de la recherche (YYYY-MM-DD HH: mm:ss)
* `reponse` est le type de réponse (individu connu ou non) 
    - 1 : connu ASG
    - 2 : connus ADPA PCH
    - 3 : pas connu
    - 4 : ambigu
* `date_deces` est la date de décès de la personne recherchée (YYYY-MM-DD)
* `lieu_deces` est le lieu de décès de la personne recherchée (- espace ')
* `date_acte` est la date de l'acte de décès de la personne recherchée (YYYY-MM-DD)
* `id_individus` renseigné uniquement si l’individu est connu, correspond à son identifiant issu de l’application tiers 
* `mdr` est le nom du référent administratif du dossier de la personne 
* `tel_mdr` est le téléphone du référent administratif 

___

## membre

La table `membre` réunit l'ensemble des membres habilités à l'application

* `id` est auto incrémenté, propre à la table
* `login` est le login du membre
* `libelle` est la description du membre
* `pass_md5` est le mot de passe du membre, encodé MD5
* `profile` est le profil du compte membre 
    - 1 : notaire
    - 3 : admin
    - 4 : métier
* `premiere_connexion` si 0 : par de connexion à l’application, si 1 : au moins une connexion à l'application
* `adresse_mail` est l'adresse mail du membre
* `echec` est le nombre d'échecs de connexion (après 3 échecs, verrouillage du compte)

___

## reinit_mdp

La table `reinit_mdp` réunit les membres de l'application dont les comptes sont verrouillés, du fait de 3 erreurs successives de mot de passe. 
ces comptes sont en attente de déblocage par réinitialisation du mot de passe.

* `id` est auto incrémenté, propre à la table
* `id_membre` est l'id du membre 
* `adresse_mail` est l'adresse mail du membre
* `tkn_cd` est un numéro généré aléatoirement envoyé sur demande pour la réinitialisation du mot de passe

