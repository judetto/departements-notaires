# Installation de l'application

Sont décrites ici les étapes pour configurer et tester rapidement l'application.

## Compétences utiles pour l'installation

* Savoir modifier les fichiers de configuration et redémarrer les services web et de base de données.
* Connaitre l'adresse IP.

## Pré-requis

* Apache 2.2 minimum
* PHP 5.6 minimum. Extensions requises :
   * pdo_mysql
   * imap 
   * mbstring
   * xml 
   * gd
* MaridaDB ou MySQL 5.7 minimum
* Composer

Il est supposé pour la suite que Apache est accessible via l'URL `http://localhost`, à adapter selon votre configuration
réelle.

### Ubuntu 16.04

```
sudo apt install \
    apache2 \
    mariadb-client \
    mariadb-server \    
    php7.0 \
    php7.0-gd \
    php7.0-imap \
    php7.0-mbstring \
    php7.0-mysql \ 
    php7.0-xml     
```


## Récupérer l'application 

* Télécharger le fichier zip contenant la version en cours : [Télécharger](https://gitlab.adullact.net/departements-notaires/departements-notaires/repository/archive.zip?ref=master)
* Dézipper le fichier et recopier son contenu dans le sous-dossier `www/notaires` du serveur Apache

Certains répertoires présents dans l'arborescence ne doivent par être servis par Apache (les directives RewriteRule l'interdisent).
Il s'agit des répertoires :
* `.private` : graphique pour personnaliser les PDF générés
* `cron` : contient le log d'import quotidien, voir `cron/cron.log`
* `documentation` : comme son nom l'indique
* `lib` : des libs PHP internes au projet
* `documentation` : comme son nom l'indique
* `pdf` : dossiers où sont générés temporairement les fichiers PDF. Les fichiers sont supprimés après envoi par email
* `test` : contient des données et outils pour les tests
* `tpl` : contient les fichiers *.tpl pour personnaliser le contenu des lettres et emails (syntaxe Twig)
* `vendor` : contient des libs PHP fournies par des tiers


## Créer la base de données

Editer le fichier `documentation/install/notaires.sql` afin de remplacer **admin@xxx.fr** par l'email valide
de l'administrateur de l'application.

En tant que root de MySQL :
* Exécuter ensuite `notaires.sql` pour créer la base `notaires` et les tables associées. 
Un utilisateur `admin` est créé par défaut avec le mot de passe `admin_notaires`.
* Créer un utilisateur MySQL `notaires`, mot de passe `notaires` ayant tous les droits sur la base `notaires`.

## Configurer à minima

Editer `config.php`: 
* Modifier les paramètres d'accès à la base de données : `$host_mysql`, `$user_mysql`, `$pass_mysql`, `$bdd_mysql`
* Modifier les paramètres pour l'envoi des emails : `$smtp_mail`, `$port_smtp_mail`, `$mail_gestion`, `$password_mail`

Editer `htaccess.php`: 
* Modifier la ligne ou ce trouve ErrorDocument 403 pour mettre l'adresse du site
* Modifier la ligne ou ce trouve ErrorDocument 404 pour mettre l'adresse du site


## Installation des dépendances

Depuis la racine du code source, lancer:

```shell
composer install --no-dev
```

## Créer des données de test

Editer `htaccess.php`: 
* Commenter la ligne RewriteRule ^test/ - [F] (ajouter un # devant)

Un échantillon de données (non réelles) est fourni pour tester l'application. Il se trouve dans le dossier `test`
dans les fichiers `demande.csv` et `individus.csv`.


Remarque : La table "Demandes" a été ajoutée à la suite des tests réalisés en interne par le service succession du Département du Rhône.
Contrairement à la table "Individus", elle ne recense pas les décisions, mais les demandes d’aides sociales en cours d’instruction et non décisionnées.
Au sein de l'organisation du Département du Rhône, elle fait l’objet d’un traitement particulier.
Mais dans le cadre d’une appropriation de la solution, cette table peut être supprimée.
En effet, les décisions et les demandes en cours d’instruction peuvent être exportées dans la même table. 
Mais attention, la requête de recherche individu fait appel à cette table, il faudra donc adapter la requête.


Afin d'importer ces fichiers, lancer à la ligne de commande depuis le dossier `test`:

``` shell
php imp_data_test.php
```

## Utiliser l'application

* Ouvrir la page d'accueil [](http://localhost/notaires) et se connecter en utilisant les identifiants `admin`/`admin_notaires`.
* Réaliser une recherche ne donnant pas de réponse
* Vérifier que la boîte de réception correspondant à l'email de l'administrateur contient un nouvel email en réponse à la recherche



 
